terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "3.0.2"
    }
  }
}

provider "docker" {}

resource "docker_image" "container_image" {
  name = "grafana/grafana"
}

resource "docker_container" "container" {
  count = 2 
  name  = "grafana_container-${count.index}"
  image = docker_image.container_image.image_id
}

output "public_ip" {
  value = [for i in docker_container.container: "${i.name} - ${i.network_data[0].ip_address}:${var.ext_port}"]
}