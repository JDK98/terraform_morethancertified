terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "3.0.2"
    }
  }
}

provider "docker" {}

resource "random_string" "random" {
  count = 2
  length  = 5
  upper   = false
  special = false
}


resource "docker_image" "node-red-image" {
  name = "nodered/node-red:latest"
}

resource "docker_container" "node-red-conteneur" {
  count = 2 
  name  =  join("-",["nodered-conteneur",random_string.random[count.index].result])
  image = docker_image.node-red-image.image_id
  ports {
    internal = "1880"
    #external = "1880"
  }
}


output "name-conteneur" {
  value = docker_container.node-red-conteneur[*].name
  description = "Conteneur"
}

output "endpoint-conteneur" {
  value       = [for i in docker_container.node-red-conteneur[*]: join(":",[i.network_data[0].ip_address],[i.ports[0].external])]
  description = "endpoint"
}


# terraform import docker_container.node-red-conteneur2 $(docker inspect --format="{{.ID}}" nodered-conteneur-5utgi)